package it.cedacri.pit.views.main;

import java.util.Optional;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentUtil;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.avatar.Avatar;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.tabs.TabsVariant;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.RouterLink;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.theme.Theme;
import it.cedacri.pit.views.main.MainView;
import it.cedacri.pit.views.home.HomeView;
import it.cedacri.pit.views.help.HelpView;
import it.cedacri.pit.views.esempi.EsempiView;
import it.cedacri.pit.views.creazionebusinesscase.CreazioneBusinessCaseView;
import it.cedacri.pit.views.copiabusinesscase.CopiaBusinessCaseView;
import it.cedacri.pit.views.archiviobusinesscase.ArchivioBusinessCaseView;
import it.cedacri.pit.views.eliminabusinesscase.EliminaBusinessCaseView;
import it.cedacri.pit.views.visualizzalistini.VisualizzaListiniView;
import it.cedacri.pit.views.visualizzadatevaliditalistino.VisualizzaDatevaliditalistinoView;

/**
 * The main view is a top-level placeholder for other views.
 */
@CssImport("./views/main/main-view.css")
@PWA(name = "pit", shortName = "pit", enableInstallPrompt = false)
@JsModule("./styles/shared-styles.js")
public class MainView extends AppLayout {

    private final Tabs menu;
    private H1 viewTitle;

    public MainView() {
        setPrimarySection(Section.DRAWER);
        addToNavbar(true, createHeaderContent());
        menu = createMenu();
        addToDrawer(createDrawerContent(menu));
    }

    private Component createHeaderContent() {
        HorizontalLayout layout = new HorizontalLayout();
        layout.setId("header");
        layout.getThemeList().set("dark", true);
        layout.setWidthFull();
        layout.setSpacing(false);
        layout.setAlignItems(FlexComponent.Alignment.CENTER);
        layout.add(new DrawerToggle());
        viewTitle = new H1();
        layout.add(viewTitle);
        layout.add(new Avatar());
        return layout;
    }

    private Component createDrawerContent(Tabs menu) {
        VerticalLayout layout = new VerticalLayout();
        layout.setSizeFull();
        layout.setPadding(false);
        layout.setSpacing(false);
        layout.getThemeList().set("spacing-s", true);
        layout.setAlignItems(FlexComponent.Alignment.STRETCH);
        Image pitLogo = new Image("images/cedacrilogo.jpg", "pit logo");
        pitLogo.setWidthFull();
        HorizontalLayout logoLayout = new HorizontalLayout();
        logoLayout.setId("logo");
        logoLayout.setAlignItems(FlexComponent.Alignment.CENTER);
        logoLayout.add(pitLogo);
        layout.add(logoLayout, menu);
        return layout;
    }

    private Tabs createMenu() {
        final Tabs tabs = new Tabs();
        tabs.setOrientation(Tabs.Orientation.VERTICAL);
        tabs.addThemeVariants(TabsVariant.LUMO_MINIMAL);
        tabs.setId("tabs");
        tabs.add(createMenuItems());
        return tabs;
    }

    private Component[] createMenuItems() {
        Anchor logout = new Anchor("logout");
        logout.add(styleIcon(new Icon(VaadinIcon.SIGN_OUT)));
        logout.add("Log Off");

        return new Tab[]{createTab(new Icon(VaadinIcon.HOME_O), "Home", HomeView.class),
                createTab(new Icon(VaadinIcon.QUESTION_CIRCLE_O), "Help", HelpView.class),
                createTab(new Icon(VaadinIcon.QUESTION_CIRCLE_O), "Esempi", EsempiView.class),
                createTab(new Icon(VaadinIcon.FOLDER_ADD), "Creazione Business Case", CreazioneBusinessCaseView.class),
                createTab(new Icon(VaadinIcon.FOLDER_ADD), "Copia Business Case", CopiaBusinessCaseView.class),
                createTab(new Icon(VaadinIcon.FOLDER_ADD), "Archivio Business Case", ArchivioBusinessCaseView.class),
                createTab(new Icon(VaadinIcon.FOLDER_REMOVE), "Elimina Business Case", EliminaBusinessCaseView.class),
                createTab(new Icon(VaadinIcon.FOLDER_O), "Visualizza Listini", VisualizzaListiniView.class),
                createTab(new Icon(VaadinIcon.FOLDER_O), "Visualizza Date validita listino", VisualizzaDatevaliditalistinoView.class),
                new Tab(logout)};
    }

    private static Tab createTab(Icon icon, String text, Class<? extends Component> navigationTarget) {
        final Tab tab = new Tab();
        RouterLink routerLink = new RouterLink(text, navigationTarget);
        routerLink.addComponentAsFirst(styleIcon(icon));
        tab.add(routerLink);
        ComponentUtil.setData(tab, Class.class, navigationTarget);
        return tab;
    }

    private static Icon styleIcon(Icon icon) {
        icon.setSize("20px");
        icon.addClassName("drawer-icons");
        return icon;
    }

    @Override
    protected void afterNavigation() {
        super.afterNavigation();
        getTabForComponent(getContent()).ifPresent(menu::setSelectedTab);
        viewTitle.setText(getCurrentPageTitle());
    }

    private Optional<Tab> getTabForComponent(Component component) {
        return menu.getChildren().filter(tab -> ComponentUtil.getData(tab, Class.class).equals(component.getClass()))
                .findFirst().map(Tab.class::cast);
    }

    private String getCurrentPageTitle() {
        PageTitle title = getContent().getClass().getAnnotation(PageTitle.class);
        return title == null ? "" : title.value();
    }
}
