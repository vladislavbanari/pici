package it.cedacri.pit.views.help;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;
import it.cedacri.pit.views.main.MainView;
import org.vaadin.alejandro.PdfBrowserViewer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Route(value = "help", layout = MainView.class)
@PageTitle("Help")
@CssImport("./views/help/help-view.css")
public class HelpView extends HorizontalLayout {

    public HelpView() {
        addClassName("help-view");
        this.setHeightFull();
        this.setWidthFull();

        StreamResource streamResource = new StreamResource("Help.pdf", this::getInputStream);
        PdfBrowserViewer viewer = new PdfBrowserViewer(streamResource);
        viewer.setHeight("100%");

        this.add(viewer);
    }

    InputStream getInputStream() {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream("./src/main/resources/docs/help.pdf");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return inputStream;
    }

}
