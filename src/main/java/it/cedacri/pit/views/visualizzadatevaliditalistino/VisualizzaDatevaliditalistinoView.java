package it.cedacri.pit.views.visualizzadatevaliditalistino;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import it.cedacri.pit.views.main.MainView;

@Route(value = "data", layout = MainView.class)
@PageTitle("Visualizza Date validita listino")
@CssImport("./views/visualizzadatevaliditalistino/visualizza-datevaliditalistino-view.css")
public class VisualizzaDatevaliditalistinoView extends HorizontalLayout {

    private TextField name;
    private Button sayHello;

    public VisualizzaDatevaliditalistinoView() {
        addClassName("visualizza-datevaliditalistino-view");
        name = new TextField("Your name");
        sayHello = new Button("Say hello");
        add(name, sayHello);
        setVerticalComponentAlignment(Alignment.END, name, sayHello);
        sayHello.addClickListener(e -> {
            Notification.show("Hello " + name.getValue());
        });
    }

}
