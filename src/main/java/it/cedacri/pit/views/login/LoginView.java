package it.cedacri.pit.views.login;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.H5;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;

@Route("login")
@PageTitle("Login | P.I.T.")
@CssImport("./views/login/login-view.css")
public class LoginView extends VerticalLayout implements BeforeEnterObserver {

	private VerticalLayout loginLayout = new VerticalLayout();
	private LoginForm loginForm = new LoginForm();
	private Image logo = new Image("images/cedacrilogo.jpg", "Logo");
	private H2 logoProject = new H2("P. I. T.");
	private H5 descriptionProject = new H5("Preventivatore Infrastruttura Tecnologica");

	public LoginView(){
		addClassName("login-view");
		setSizeFull();
		setAlignItems(Alignment.CENTER);
		setJustifyContentMode(JustifyContentMode.CENTER);

		loginForm.setAction("login");
		loginForm.setForgotPasswordButtonVisible(false);
		loginForm.getElement().getStyle().set("margin-bottom", "-20px");

		loginLayout.add(logo, logoProject, descriptionProject, loginForm);
		loginLayout.setAlignItems(Alignment.CENTER);
		loginLayout.addClassName("login-layout");
		loginLayout.getStyle().set("width", "auto");

		add(loginLayout);
	}

	@Override
	public void beforeEnter(BeforeEnterEvent beforeEnterEvent) {
		// inform the user about an authentication error
		if(beforeEnterEvent.getLocation()
				.getQueryParameters()
				.getParameters()
				.containsKey("error")) {
			loginForm.setError(true);
		}
	}
}