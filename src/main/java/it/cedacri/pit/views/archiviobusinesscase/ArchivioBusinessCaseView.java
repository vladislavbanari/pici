package it.cedacri.pit.views.archiviobusinesscase;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import it.cedacri.pit.views.main.MainView;

@Route(value = "archivio", layout = MainView.class)
@PageTitle("Archivio Business Case")
@CssImport("./views/archiviobusinesscase/archivio-business-case-view.css")
public class ArchivioBusinessCaseView extends HorizontalLayout {

    private TextField name;
    private Button sayHello;

    public ArchivioBusinessCaseView() {
        addClassName("archivio-business-case-view");
        name = new TextField("Your name");
        sayHello = new Button("Say hello");
        add(name, sayHello);
        setVerticalComponentAlignment(Alignment.END, name, sayHello);
        sayHello.addClickListener(e -> {
            Notification.show("Hello " + name.getValue());
        });
    }

}
